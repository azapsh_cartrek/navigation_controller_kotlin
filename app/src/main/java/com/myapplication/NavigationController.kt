package com.myapplication

import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class NavigationController {
    private var body: RelativeLayout? = null
    private var fragmentManager: FragmentManager? = null
    private var rootView: Fragment? = null
    private val stack = arrayListOf<Fragment>()

    fun setup(fragmentManager: FragmentManager, rootView: Fragment, body: RelativeLayout) {
        this.rootView = rootView
        this.fragmentManager = fragmentManager
        this.body = body

        val tr = fragmentManager.beginTransaction()
        tr.add(body.id, rootView)
        tr.commitAllowingStateLoss()
    }

    fun push(fragmentView: Fragment) {
        val tr = fragmentManager?.beginTransaction()?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        tr?.add(body!!.id, fragmentView)
        stack.add(fragmentView)
        tr?.commitAllowingStateLoss()
    }

    fun popOver() {
        val tr = fragmentManager?.beginTransaction()?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        val body = body?.let { it } ?: return
        if (body.isEmpty()) { return }
        if (stack.size == 0) { return }
        val last = stack.get( stack.size - 1);
        stack.remove(last)
        tr?.remove(last)?.commit()
    }

    fun popToRoot() {
        val tr = fragmentManager?.beginTransaction()
        stack.forEach {
            tr?.remove(it)
        }
        stack.clear()
        tr?.commit()
    }

    fun backPressed(): Boolean {
        if (stack.size == 0) {
            return true
        } else {
            popOver()
            return false
        }
    }

    companion object {
        val instance = NavigationController()
    }
}